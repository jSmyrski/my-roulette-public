package pl.jsmyrski.casino.login;

import pl.jsmyrski.casino.login.exceptions.InvalidEmailException;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {

    private static final String EMAIL_REGEX  = "^[A-Z0-9_-]+@[A-Z0-9]+\\.[A-Z]{2,10}$";
    private Matcher matcher;
    private Pattern pattern;

    public User validateUser(int userID, String userName, String userEmail, String userPassword){
            if (userName != null && userName.length() > 0 && userEmail != null && userPassword != null && userID > 0) {
                User user = new User(userID);
                user.setUserName(userName);
                user.setUserPassword(userPassword);
                if (validateEmail(userEmail)){
                    user.setUserEmail(userEmail);
                    return user;
                }else{
                    throw new InvalidEmailException("That's definitely not an email!");
                }
            }
        return null;
    }
    public boolean validateEmail (String email){
        pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(email);
        return matcher.find();
    }
}

