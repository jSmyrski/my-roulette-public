package pl.jsmyrski.casino.login;

import pl.jsmyrski.casino.login.exceptions.InvalidEmailException;

import java.util.HashMap;
import java.util.Map;

public class UserService {

    Map<Integer, User> users = new HashMap<>();
    UserValidator userValidator = new UserValidator();

    public User createUser(int userId, String userName, String userEmail, String password){
        if (!users.containsKey(userId)){
            User user = userValidator.validateUser(userId, userName, userEmail, password);
            users.put(user.getUserID(),user);
            return user;
        }
        return null;
    }

    public boolean changeEmail(String newEmail, int userID){
        if (users.containsKey(userID)){
            if (userValidator.validateEmail(newEmail)){
                users.get(userID).setUserEmail(newEmail);
                return true;
            }else{
                throw new InvalidEmailException("That's definitely not an email!");
            }
        }
        return false;
    }

    public Map<Integer, User> getUsers(){
        return new HashMap<>(users);
    }

    public User getUser(int userID){
        if (users.containsKey(userID)){
            return users.get(userID);
        }
        return null;
    }

    public boolean userExist(int userID){
        return users.containsKey(userID);
    }
}
