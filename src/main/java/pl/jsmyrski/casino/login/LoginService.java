package pl.jsmyrski.casino.login;

import pl.jsmyrski.casino.dto.LoginDto;
import pl.jsmyrski.casino.login.exceptions.NoSuchUserException;
import pl.jsmyrski.casino.login.exceptions.WrongEmailOrPasswordException;

import java.util.Map;
import java.util.OptionalInt;

public class LoginService {

    UserService userService = new UserService();

    public LoginDto login(String userEmail, String userPassword){
        Map <Integer, User> checkMap = userService.getUsers();
        LoginDto dto = new LoginDto();
        checkMap.forEach((id, user) -> {
            if (user.getUserEmail().equalsIgnoreCase(userEmail) && user.getUserPassword().equals(userPassword)) {
                dto.setUserPassword(user.getUserPassword());
                dto.setUserEmail(user.getUserEmail());
                dto.setUserName(user.getUserName());
                dto.setUserID(id);
            }else if(user.getUserEmail().equalsIgnoreCase(userEmail) && !user.getUserPassword().equals(userPassword)){
                throw new WrongEmailOrPasswordException("Wrong email or password!");
            }else if (!user.getUserEmail().equalsIgnoreCase(userEmail) && user.getUserPassword().equals(userPassword)){
                throw new WrongEmailOrPasswordException("Wrong email or password!");
            }
        });
        if (dto.getUserName() == null || dto.getUserEmail() == null || dto.getUserPassword() == null){
            throw new NoSuchUserException("There is no user like this.");
        }
        return dto;
    }

    public LoginDto register(String userName, String userEmail, String userPassword){
        OptionalInt maxId = userService.getUsers().keySet().stream().mapToInt(Integer::intValue).max();
        int nextEmptyId = maxId.orElse(0) + 1;

        User user = userService.createUser(nextEmptyId, userName, userEmail, userPassword);
        LoginDto dto = new LoginDto();
        dto.setUserID(user.getUserID());
        dto.setUserPassword(user.getUserPassword());
        dto.setUserEmail(user.getUserEmail());
        dto.setUserName(user.getUserName());
        return dto;
    }
}
