package pl.jsmyrski.casino.login;

import pl.jsmyrski.casino.dto.LoginDto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User {

    private final int userID;
    private String userName;
    private String userEmail;
    private String userPassword;

    public User(int userID) {
        this.userID = userID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

}
