package pl.jsmyrski.casino.login.exceptions;

public class NoSuchUserException extends RuntimeException{
    public NoSuchUserException(String message) { super(message); }
}
