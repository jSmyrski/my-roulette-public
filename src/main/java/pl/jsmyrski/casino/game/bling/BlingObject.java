package pl.jsmyrski.casino.game.bling;

public class BlingObject {
    private int rangeFrom;
    private int rangeTo;
    private String name;
    private int multipiler;
    public BlingObject (int rangeFrom , int rangeTo, String name, int multipiler) {
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.name = name;
        this.multipiler = multipiler;
    }


    public int getMultipiler() { return multipiler; }
    public int getRangeFrom() { return rangeFrom; }
    public int getRangeTo() { return rangeTo; }
    public String getName() { return name; }
}
