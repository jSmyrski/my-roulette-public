package pl.jsmyrski.casino.game.bling;

import pl.jsmyrski.casino.game.Game;
import pl.jsmyrski.casino.pocket.Portfel;
import pl.jsmyrski.casino.ConsoleStartMenu;

import java.util.Random;
import java.util.Scanner;

public class Bling extends ConsoleStartMenu implements Game {
    private static final BlingObject SIEDEM = new BlingObject(0, 4, "Siedem", 20);
    private static final BlingObject WISNIA = new BlingObject(5, 14, "Wiśnia", 5);
    private static final BlingObject MONETA = new BlingObject(15, 29, "Moneta", 3);
    private static final BlingObject BANKNOT = new BlingObject(30, 49, "Banknoty", 2);
    private static final BlingObject KORONA = new BlingObject(50, 74, "Korona", 2);
    private static final BlingObject SZTABKA = new BlingObject(75, 99, "Sztabka", 2);
    private static final BlingObject[] AVAIABLE_OBJECTS = {SIEDEM, WISNIA, MONETA, BANKNOT, KORONA, SZTABKA};

    private static final BlingPuzzle one = new BlingPuzzle("one");
    private static final BlingPuzzle two = new BlingPuzzle("two");
    private static final BlingPuzzle three = new BlingPuzzle("three");
    private static final BlingPuzzle four = new BlingPuzzle("four");
    private static final BlingPuzzle five = new BlingPuzzle("five");
    private static final BlingPuzzle six = new BlingPuzzle("six");
    private static final BlingPuzzle seven = new BlingPuzzle("seven");
    private static final BlingPuzzle eight = new BlingPuzzle("eight");
    private static final BlingPuzzle nine = new BlingPuzzle("nine");
    private static final BlingPuzzle[] AVAIABLE_PUZZLES = {one, two, three, four, five, six, seven, eight, nine};
    public void bling(Portfel portfel) {
        String zakladS;
        Scanner skan = new Scanner(System.in);
        Random rnd = new Random();
        System.out.println("Witaj w Bling!");
        while (true) {
            if (!portfel.hasMoney()) {
                System.out.println("Brak środków na koncie!");
                return;
            }
            portfel.printSaldo();
            portfel.printBlingStarter();



            zakladS = skan.next();
            if (zakladS.equalsIgnoreCase("Koniec")){
                return;
            }
            int zaklad = Integer.parseInt(zakladS);

            while (isZakladCorrect(zaklad, portfel.getSaldo())){
                if (zaklad > 0 && zaklad <= portfel.getSaldo()) {
                    break;
                }
                System.out.println("Podana wartość jest niepoprawna!");
                zaklad = skan.nextInt();
            }
            System.out.println("Maszyna zaczyna losować symbole!");

            for (BlingPuzzle puzzle : AVAIABLE_PUZZLES) {
                BlingObject guess = spinMachine(rnd);
                puzzle.show = guess.getName();
                puzzle.multipiler = guess.getMultipiler();
            }
            System.out.print("[" + one.show + "]" +" "); System.out.print("[" + two.show + "]" + " "); System.out.println("[" + three.show + "]" + " ");
            System.out.print("[" + four.show + "]" + " "); System.out.print("[" + five.show + "]" + " "); System.out.println("[" + six.show + "]" + " ");
            System.out.print("[" + seven.show + "]" + " "); System.out.print("[" + eight.show + "]" + " "); System.out.println("[" + nine.show + "]" + " ");
            boolean win = false;
            if (one.show == two.show && two.show == three.show) {
                portfel.addMoney(comparePuzzles(one,zaklad));
                win = true;
            }
            if (four.show == five.show && five.show == six.show) {
                portfel.addMoney(comparePuzzles(four,zaklad));
                win = true;
            }
            if (seven.show == eight.show && eight.show == nine.show) {
                portfel.addMoney(comparePuzzles(seven,zaklad));
                win = true;
            }
            if (one.show == four.show && four.show == seven.show) {
                portfel.addMoney(comparePuzzles(one, zaklad));
                win = true;
            }
            if (two.show == five.show && five.show == eight.show) {
                portfel.addMoney(comparePuzzles(two,zaklad));
                win = true;
            }
            if (three.show == six.show && six.show == nine.show) {
                portfel.addMoney(comparePuzzles(three,zaklad));
                win = true;
            }
            if (one.show == five.show && five.show == nine.show) {
                portfel.addMoney(comparePuzzles(one,zaklad));
                win = true;
            }
            if (three.show == five.show && five.show == seven.show) {
                portfel.addMoney(comparePuzzles(three,zaklad));
                win = true;
            }
            if (!win) {
                System.out.println("Nie wylosowałeś nic, przegrałes " + zaklad);
                portfel.deductMoney(zaklad);
            }
        }

    }

    private int comparePuzzles(BlingPuzzle puzzle, int zaklad) {
        System.out.println("Brawo, wylosowałeś trzy " + puzzle.show);
        System.out.println("Wygrałeś " + (zaklad * puzzle.multipiler));
        int wygrana = zaklad * puzzle.multipiler;
        return wygrana;
    }

    private boolean isZakladCorrect(int x , int y) {
        return true;
    }

    private BlingObject spinMachine(Random rnd ) {
        int Random = rnd.nextInt(99);
        for (BlingObject object : AVAIABLE_OBJECTS) {
            if (object.getRangeFrom() <= Random && object.getRangeTo() >= Random) {
                return object;
            }
        }
        return null;
    }

    @Override
    public void run(Portfel portfel) {
        bling(portfel);
    }


}
