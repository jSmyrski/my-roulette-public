package pl.jsmyrski.casino.game;

import pl.jsmyrski.casino.pocket.Portfel;

public interface Game {
    void run(Portfel portfel);
}
