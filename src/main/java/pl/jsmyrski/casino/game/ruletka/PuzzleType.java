package pl.jsmyrski.casino.game.ruletka;

public class PuzzleType {

    private int mnoznik;
    private String show;
    private int wygrana;
    private int saldo;
    private int zaklad;

    public PuzzleType (int mnoznik, String show, int wygrana, int saldo, int zaklad)
    {
        this.mnoznik = mnoznik;
        this.show = show;
        this.wygrana = wygrana;
        this.saldo = saldo;
        this.zaklad = zaklad;
    }

    public int getMnoznik() { return mnoznik; }
    public int getSaldo() { return saldo; }
    public int getWygrana() { return wygrana; }
    public int getZaklad() { return zaklad; }
    public String getShow() { return show; }
}
