package pl.jsmyrski.casino.game.ruletka;

import pl.jsmyrski.casino.dto.RuletteResultDto;
import pl.jsmyrski.casino.pocket.Portfel;
import java.util.Random;

public class Ruletka {

    private static final LosOption CZARNY = new LosOption(0, 44, 1, 2, "czarny");
    private static final LosOption CZERWONY = new LosOption(45, 89, 2, 2, "czerwony");
    private static final LosOption ZIELONY = new LosOption(90, 99, 3, 10, "zielony");

    private static final LosOption[] AVAILABLE_OPTIONS = {CZARNY, CZERWONY, ZIELONY};

    public RuletteResultDto spinRoulette(Portfel portfel, String color, Integer zaklad) {
        Random rnd = new Random();
        RuletteResultDto rouletteResultDto = new RuletteResultDto();

        while (true) {
            if (!portfel.hasMoney()){
                System.out.println("Nie masz środków na koncie.");
                return null;
            }

            LosOption guess = spinRoulette(rnd);

            portfel.printSaldo();

            if (color.equalsIgnoreCase(guess.getName())) {
                int wygrana = zaklad * guess.getMultiplayer();
                rouletteResultDto.setWon(true);
                rouletteResultDto.setWonAmount(wygrana);
            }
            else {
                rouletteResultDto.setWon(false);
                rouletteResultDto.setWonAmount(-(zaklad));
            }
            rouletteResultDto.setDrawnColor(guess.getName());
            rouletteResultDto.setChoosenColor(color);
            portfel.addMoney(rouletteResultDto.getWonAmount());
            rouletteResultDto.setPocketAmount(portfel.getSaldo());
            return rouletteResultDto;
        }
    }


    private LosOption spinRoulette(Random rnd) {
        int random = rnd.nextInt(99);

        for(LosOption option : AVAILABLE_OPTIONS) {
            if (option.getRangeFrom() <= random && option.getRangeTo() >= random) {
                return option;
            }
        }
        return null;
    }

}

