package pl.jsmyrski.casino.game.ruletka;

public class LosOption {

    private int rangeFrom;
    private int rangeTo;
    private int optionNumber;
    private int multiplayer;
    private String name;

    public LosOption(int rangeFrom, int rangeTo, int optionNumber, int multiplayer, String name) {
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.optionNumber = optionNumber;
        this.multiplayer = multiplayer;
        this.name = name;
    }

    public int getRangeFrom() {
        return rangeFrom;
    }

    public int getRangeTo() {
        return rangeTo;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public int getMultiplayer() {
        return multiplayer;
    }

    public String getName() {
        return name;
    }
}


