package pl.jsmyrski.casino;

import pl.jsmyrski.casino.dto.RuletteResultDto;
import pl.jsmyrski.casino.game.bling.Bling;
import pl.jsmyrski.casino.game.Game;
import pl.jsmyrski.casino.game.ruletka.LosOption;
import pl.jsmyrski.casino.game.ruletka.Ruletka;
import pl.jsmyrski.casino.pocket.Portfel;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleStartMenu {
    private static final LosOption CZARNY = new LosOption(0, 44, 1, 2, "czarny");
    private static final LosOption CZERWONY = new LosOption(45, 89, 2, 2, "czerwony");
    private static final LosOption ZIELONY = new LosOption(90, 99, 3, 10, "zielony");
    private static final LosOption KONIEC = new LosOption(0, 0, 0, 0, "Koniec");

    private static final LosOption[] AVAILABLE_OPTIONS = {CZARNY, CZERWONY, ZIELONY};


    public void startMenu(){
        Ruletka ruletka = new Ruletka();
        Scanner skan = new Scanner(System.in);
        String o1 = "Ruletka";
        String o2 = "Bling";
        String chs;

        Portfel portfel = null;

        System.out.println("Witaj w kasynie!");
        showNoMoneyMenu();
        chs = skan.nextLine();
        String oo1 = "Wplacam";
        String oo2 = "Koniec";
        while (true){
            if (chs.equalsIgnoreCase(oo1)){
                break;
            }
            if (chs.equalsIgnoreCase(oo2)){
                break;
            }
            System.out.println("Niepoprawna wartość!");
            showNoMoneyMenu();
            chs = skan.nextLine();
        }
        if (chs.equalsIgnoreCase(oo1)) {
            portfel = new Portfel(depositMoney(false, skan));
        }
        if (chs.equalsIgnoreCase(oo2)) {
            return;
        }

        while (true) {
            if (!portfel.hasMoney()) {
                showNoMoneyMenu();
                portfel.addMoney(depositMoney(false, skan));
            }

            showMenu();
            chs = skan.next();
            while (true){
                if (chs.equalsIgnoreCase(o1)){
                    break;
                }
                if (chs.equalsIgnoreCase(o2)){
                    break;
                }
                if (chs.equalsIgnoreCase("Koniec")) {
                    break;
                }
                System.out.println("Niepoprawna wartość!");
                showMenu();
                chs = skan.next();
            }
            Game game = null;
            if (chs.equalsIgnoreCase(o1)) {
                System.out.println("Witaj w ruletce!");
                while(true) {
                    if (portfel.getSaldo()<=0){
                        return;
                    }
                    System.out.println("Na jaki kolor chcesz obstawić?");
                    mainMenu();
                    String los = readUserOption(skan);
                    if (los.equalsIgnoreCase(KONIEC.getName())){
                        return;
                    }
                    portfel.printSaldo();
                    System.out.println("Ile chcesz obstawić?");
                    String zaklads = skan.next();
                    while (!isZakladCorrect(zaklads, portfel.getSaldo())) {
                        System.out.println("Podana wartość jest niepoprawna!");
                        zaklads = skan.next();
                    }
                    int zaklad = Integer.parseInt(zaklads);
                    while (true) {
                        if (zaklad > 0 && zaklad <= portfel.getSaldo()) {
                            break;
                        }
                        System.out.println("Podana wartość jest niepoprawna!");
                        zaklad = skan.nextInt();
                    }
                    RuletteResultDto guess = ruletka.spinRoulette(portfel, los, zaklad);
                    System.out.println("Wylosowany kolor to " + guess.getDrawnColor());
                    if (guess.isWon()) {
                        System.out.println("Wylosowany kolor zgadza się z twoim zakładem, wygrywasz " + guess.getWonAmount() + "zł.");
                    } else {
                        System.out.println("Niestety nie trafiłeś, przegrałeś " + -(guess.getWonAmount()) + "zł.");
                    }
                }
            }
            if (chs.equalsIgnoreCase(o2)) {
                game = new Bling();
            }
            if (chs.equalsIgnoreCase("Koniec")) {
                return;
            }
            game.run(portfel);
        }

    }

    private void showAddMoneyMenu() {
        System.out.println("Ile pieniedzy chcesz wplacic?");
    }

    private int depositMoney(boolean hasMoney, Scanner skan){
            while(!hasMoney){
                showAddMoneyMenu();
                String next = skan.nextLine();
                try{
                    int i = Integer.parseInt(next);
                    if (i>0){
                        hasMoney = true;
                        return i;
                    }
                    System.out.println("Niepoprawna wartość!");
                }catch (NumberFormatException e){
                    System.out.println("Niepoprawna wartość!");
                }
            }
        return -1;
    }

    private void showNoMoneyMenu() {
        System.out.println("Nie masz pieniędzy na koncie, wybierz co chcesz zrobić:");
        System.out.println("1. Wplacam");
        System.out.println("2. Koniec");
    }

    private static void showMenu() {
        System.out.println("W jaką grę chcesz zagrać?");
        System.out.println("Ruletka");
        System.out.println("Bling");
        System.out.println("Jeśli chcesz zakończyć wybierz: Koniec");
    }
    private void mainMenu() {
        for (LosOption option : AVAILABLE_OPTIONS) {
            String name = option.getName().substring(0,1).toUpperCase() + option.getName().substring(1, option.getName().length());
            System.out.println(name);
        }

    }
    private String readUserOption(Scanner skan) {
        String input = skan.next();

        for (LosOption option : AVAILABLE_OPTIONS) {
            if (option.getName().equalsIgnoreCase(input)) {
                return option.getName();
            }
        }
        if (input.equalsIgnoreCase("Koniec")) {
            return KONIEC.getName();
        }
        return null;
    }
    private boolean isZakladCorrect(String x , int y) {
        int zaklad = 0;
        try{
            zaklad = Integer.parseInt(x);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }
}

