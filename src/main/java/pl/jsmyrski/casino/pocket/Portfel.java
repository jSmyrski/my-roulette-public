package pl.jsmyrski.casino.pocket;

import pl.jsmyrski.casino.dto.PocketDto;

public class Portfel {

    private int saldo;

    public Portfel(int saldo) {
        this.saldo = saldo;
    }

    public int getSaldo() {
        return saldo;
    }

    public PocketDto addMoney(int moneyToAdd) {
        this.saldo = saldo + moneyToAdd;
        PocketDto pocketDto = new PocketDto();
        pocketDto.setAmount(saldo);
        return pocketDto;
    }

    public void deductMoney(int moneyToDeduct) {
        this.saldo = saldo - moneyToDeduct;
    }

    public boolean hasMoney() {
        return saldo > 0;
    }

    public void printSaldo(){
        System.out.println("Saldo twojego konta wynosi " + saldo);
    }

    public void printBlingStarter(){
        System.out.println("Jeżeli chcesz zakończyć rozgrywkę, wybierz 'Koniec' ");
        System.out.println("Ile pieniędzy chcesz obstawić?");
    }
}
