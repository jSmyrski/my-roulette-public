package pl.jsmyrski.casino.pocket;

import java.util.HashMap;
import java.util.Map;

public class PocketService {

    private static Map<Integer, Portfel> usersPockets = new HashMap<>();

    public Portfel getUserPocket(int userId) {
        return usersPockets.get(userId);
    }
    public boolean userPocketExists(int userID){
        if(usersPockets.containsKey(userID)){
            return true;
        }
        return false;
    }
    public Portfel createUserPocket(int userId){
        Portfel pocket = new Portfel(100);
        usersPockets.put(userId, pocket);
        return usersPockets.get(userId);
    }


}
