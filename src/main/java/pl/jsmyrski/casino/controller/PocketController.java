package pl.jsmyrski.casino.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.jsmyrski.casino.dto.PocketDto;
import pl.jsmyrski.casino.pocket.PocketService;
import pl.jsmyrski.casino.pocket.Portfel;

@RequestMapping("/user/pocket")
@Controller
@CrossOrigin//nie wiem co oznacza ta adnotacja
public class PocketController {

    private static Logger logger = LoggerFactory.getLogger(PocketController.class);

    private PocketService pocketService = new PocketService();

    @RequestMapping(value = "/adjustPocket", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PocketDto> adjustPocket(
            @RequestParam("userID") Integer userID,
            @RequestParam("value") Integer value) {
        if (!pocketService.userPocketExists(userID)){
            logger.info("Creating new pocket for user with ID = {}" + userID);
            pocketService.createUserPocket(userID);
        }else{
            logger.info("Setting pocket value for user with ID = {}" + userID);
        }
        Portfel portfel = pocketService.getUserPocket(userID);
        PocketDto pocket = portfel.addMoney(value);
        return ResponseEntity.ok(pocket);
    }
}
//TODO http://localhost:8080/user/pocket/adjustPocket?userID=1&value=200
