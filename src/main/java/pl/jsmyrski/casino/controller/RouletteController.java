package pl.jsmyrski.casino.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.jsmyrski.casino.dto.RuletteResultDto;
import pl.jsmyrski.casino.game.ruletka.Ruletka;
import pl.jsmyrski.casino.pocket.PocketService;
import pl.jsmyrski.casino.pocket.Portfel;

@RequestMapping("/game/roulette")
@Controller
@CrossOrigin
public class RouletteController {
    private static Logger logger = LoggerFactory.getLogger(RouletteController.class);

    private PocketService pocketService = new PocketService();

    @RequestMapping(value = "/spinRoulette", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RuletteResultDto> spinRoulette(
            @RequestParam("userID") Integer userID,
            @RequestParam("color") String color,
            @RequestParam("amount") Integer amount) {
        logger.info("spinRoulette for user with id = {}" + userID);
        if (!pocketService.userPocketExists(userID)){
            pocketService.createUserPocket(userID);
        }
        Portfel portfel = pocketService.getUserPocket(userID);
        Ruletka ruletka = new Ruletka();
        RuletteResultDto guess = ruletka.spinRoulette(portfel, color, amount);
        return ResponseEntity.ok(guess);
    }
    //TODO http://localhost:8080/game/roulette/spinRoulette?userID=1&color=czerwony&amount=300
}
