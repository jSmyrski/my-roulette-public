package pl.jsmyrski.casino.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.jsmyrski.casino.dto.LoginDto;
import pl.jsmyrski.casino.login.LoginService;


@RequestMapping("/user/logging")
@Controller
@CrossOrigin
public class LoginController {

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);
    LoginService ls = new LoginService();


    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginDto> register(
            @RequestParam("userName") String userName,
            @RequestParam("userEmail") String userEmail,
            @RequestParam("userPassword") String userPassword){
        LoginDto loginDto = ls.register(userName, userEmail, userPassword);
        logger.info("Registering new user with id {}" + loginDto.getUserID());
        return ResponseEntity.ok(loginDto);
    }

    @PostMapping(value="/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginDto> login(
            @RequestParam("userEmail") String userEmail,
            @RequestParam("userPassword") String userPassword){
        LoginDto loginDto = ls.login(userEmail, userPassword);
        logger.info("Logging already existing user with id {}" + loginDto.getUserID());
        return ResponseEntity.ok(loginDto);
    }
    //TODO rejestracja http://localhost:8080/user/logging/register
    //TODO logowanie http://localhost:8080/user/logging/login
}
