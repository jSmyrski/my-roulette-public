package pl.jsmyrski.casino;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAppRunner {

    private static Logger logger = LoggerFactory.getLogger(WebAppRunner.class);

    public static void main(String[] args) {
        SpringApplication.run(WebAppRunner.class, args);
        logger.info("Casino webapp started!");
    }

}
