package pl.jsmyrski.casino.dto;

public class RuletteResultDto {

    private String drawnColor;
    private String choosenColor;
    private boolean won;
    private Integer wonAmount;
    private Integer pocketAmount;


    public String getDrawnColor() {
        return drawnColor;
    }

    public void setDrawnColor(String drawnColor) {
        this.drawnColor = drawnColor;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public Integer getWonAmount() {
        return wonAmount;
    }

    public void setWonAmount(Integer wonAmount) {
        this.wonAmount = wonAmount;
    }

    public Integer getPocketAmount() {
        return pocketAmount;
    }

    public void setPocketAmount(Integer pocketAmount) {
        this.pocketAmount = pocketAmount;
    }

    public String getChoosenColor() { return choosenColor; }

    public void setChoosenColor(String choosenColor) { this.choosenColor = choosenColor; }
}
