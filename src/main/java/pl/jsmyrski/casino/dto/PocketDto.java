package pl.jsmyrski.casino.dto;

public class PocketDto {

    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public Integer setAmount(Integer amount) {
        this.amount = amount;
        return amount;
    }
}
