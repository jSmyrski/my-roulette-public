package pl.jsmyrski.casino.pocket;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PocketServiceTest {
    PocketService ps;

    @Before
    public void setup(){
        ps = new PocketService();
        ps.createUserPocket(7);
    }
    @Test
    public void getPocket_validPocketData() {
        assertEquals(100, ps.getUserPocket(7).getSaldo());
        ps.getUserPocket(7).getSaldo();
    }

    @Test
    public void getPocket_invalidPocketData(){
        assertNull("Oops, something went really wrong right here!" , ps.getUserPocket(1));
    }

    //TODO zaimplementuj swoj wyjatek zamiast nullpointera
    @Test(expected = NullPointerException.class)
    public void getPocket_nonExistingPocketTest(){
        ps.getUserPocket(8).getSaldo();
        ps.getUserPocket(1).getSaldo();
    }
    @Test
    public void isSet() {
        assertEquals(true, ps.userPocketExists(7));
        assertEquals(false, ps.userPocketExists(0));
    }

    @Test
    public void setPocket() {
        assertEquals(100, ps.createUserPocket(8).getSaldo());
        assertNotEquals(99, ps.createUserPocket(2).getSaldo());
    }
}