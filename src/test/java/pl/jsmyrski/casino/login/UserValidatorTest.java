package pl.jsmyrski.casino.login;

import org.junit.Before;
import org.junit.Test;
import pl.jsmyrski.casino.login.exceptions.InvalidEmailException;

import static org.junit.Assert.*;

public class UserValidatorTest {

    UserValidator valid;

    @Before
    public void setup(){
        valid = new UserValidator();
    }
    @Test
    public void validateUser_validUser() {
        User user = valid.validateUser(1, "Serek", "someMail@gmail.com", "12345");
        assertEquals(1, user.getUserID());
        assertEquals("Serek", user.getUserName());
        assertEquals("12345", user.getUserPassword());
        assertEquals("someMail@gmail.com", user.getUserEmail());
    }
    @Test(expected = InvalidEmailException.class)
    public void validateUser_invalidUser(){
        valid.validateUser(2, "invalidUser", "thisIsNotAnEmail", "someWeirdPassword");
    }
}