package pl.jsmyrski.casino.login;

import org.junit.Before;
import org.junit.Test;
import pl.jsmyrski.casino.login.exceptions.InvalidEmailException;

import java.util.Map;

import static org.junit.Assert.*;

public class UserServiceTest {

    UserService userService;
    User validUser;

    @Before
    public void setUp(){
        userService = new UserService();
        validUser = userService.createUser(1, "Jacob", "jacob654@gmail.com", "12345");
    }

    @Test
    public void createUser_validMail() {
        assertEquals("John", userService.createUser(2, "John", "johnBohn@hotmail.com", "passW0RD").getUserName());
    }
    @Test(expected = InvalidEmailException.class)
    public void createUser_invalidMail(){
        userService.createUser(99, "ThisWon'tWork", "invalidMail", "definitelyNotAnPassW");
    }

    @Test
    public void changeEmail_validMail(){
        userService.changeEmail("big_john99@gmail.com", validUser.getUserID());
        assertEquals("big_john99@gmail.com", validUser.getUserEmail());
        assertEquals(false, userService.changeEmail("notAnEmail", 15));
   }

   @Test(expected = InvalidEmailException.class)
    public void changeEmail_IncorrectMail(){
       userService.changeEmail("invalid mail", validUser.getUserID());
   }

    @Test
    public void getUsers_properMapImplementation() {
        Map<Integer, User> userMap= userService.getUsers();
        assertEquals("Jacob", userMap.get(1).getUserName());
    }
    @Test
    public void getUsers_tryToChangeInternalMap(){
        User robber = new User(2);
        robber.setUserEmail("immaRobYou:)");
        robber.setUserName("robber");
        robber.setUserPassword("notAPassword");
        userService.getUsers().put(2, robber);
        assertEquals(false, userService.userExist(robber.getUserID()));
    }
    @Test
    public void getUser_existingUser() {
        assertEquals("jacob654@gmail.com", userService.getUser(validUser.getUserID()).getUserEmail());
    }
    @Test
    public void getUser_nonExistingUser(){
        assertNull("oops, something went wrong", userService.getUser(1500));
    }
    @Test
    public void isSet_existingUser() {
        assertEquals(true, userService.userExist(validUser.getUserID()));
    }
    @Test
    public void isSet_nonExistingUser(){
        assertEquals(false, userService.userExist(9));
    }
}