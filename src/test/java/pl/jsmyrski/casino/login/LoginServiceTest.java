package pl.jsmyrski.casino.login;

import org.junit.Before;
import org.junit.Test;
import pl.jsmyrski.casino.dto.LoginDto;
import pl.jsmyrski.casino.login.exceptions.InvalidEmailException;
import pl.jsmyrski.casino.login.exceptions.NoSuchUserException;
import pl.jsmyrski.casino.login.exceptions.WrongEmailOrPasswordException;

import static org.junit.Assert.*;

public class LoginServiceTest {

    LoginService ls;

    @Before
    public void setUp() {
        ls = new LoginService();
        ls.register("John", "john10@gmail.com", "passW0RD");
    }

    @Test
    public void login_validUserData() {
       LoginDto dto =  ls.login("john10@gmail.com", "passW0RD");
       assertEquals("John", dto.getUserName());
    }

    @Test(expected = WrongEmailOrPasswordException.class)
    public void login_invalidUserEmail(){
        ls.login("wrong@email.com", "passW0RD");
    }

    @Test(expected = WrongEmailOrPasswordException.class)
    public void login_invalidUserPassword(){
        ls.login("john10@gmail.com", "wrongPASSWORD");
    }

    @Test(expected = NoSuchUserException.class)
    public void login_invalidUserData(){
        ls.login("wrong@email.com", "wrongPASSWORD");
    }

    @Test
    public void register_validUserEmail() {
        LoginDto dto = ls.register("Serek", "serek123@interia.pl", "12345");
        ls.register("John", "john@hotmail.com", "gsojjs23");
        ls.register("Kuba", "buba123@interia.pl", "1daagaa2345");
        assertEquals("Serek", dto.getUserName());
        assertEquals("serek123@interia.pl", dto.getUserEmail());
    }
    @Test(expected = InvalidEmailException.class)
    public void register_invalidUserEmail(){
        ls.register("InvalidUser", "InvalidMail", "InvalidPassword");
    }
}